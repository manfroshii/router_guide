import 'dart:async';
import 'package:angular/angular.dart';
import 'package:angular_forms/angular_forms.dart';
import 'package:angular_router/angular_router.dart';

import 'crisis.dart';
import 'crisis_service.dart';
import 'dialog_service.dart';
import 'route_paths.dart';

@Component(
  selector: 'my-crisis',
  templateUrl: 'crisis_component.html',
  styleUrls: ['crisis_component.css'],
  directives: [coreDirectives, formDirectives],
)
class CrisisComponent extends Object
    with CanReuse implements OnActivate, OnDeactivate, CanNavigate {
  Crisis crisis;
  final CrisisService _crisisService;
  final DialogService _dialogService; 
  final Router _router;
  String name;
  
  CrisisComponent(this._crisisService, this._router, this._dialogService) {
  print('CrisisComponent: created');
  }

  @override
  void onActivate(_, RouterState current) async {
    print('CrisisComponent: onActivate: ${_?.toUrl()} -> ${current?.toUrl()}');
    final id = getId(current.parameters);
    if (id == null) return null;
    crisis = await (_crisisService.get(id));
    name = crisis?.name;
  }

  void onDeactivate(RouterState current, _) {
  print('CrisisComponent: onDeactivate: ${current?.toUrl()} -> ${_?.toUrl()}');
  }

  Future<void> save() async {
  crisis?.name = name;
  goBack();
  }
Future<NavigationResult> goBack() =>
    _router.navigate(RoutePaths.home.toUrl());

  Future<bool> canNavigate() async {
  print('CrisisComponent: canNavigate');
  return crisis?.name == name ||
      await _dialogService.confirm('Discard changes?');
}
}